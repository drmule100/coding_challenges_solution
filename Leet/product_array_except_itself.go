package leet
/* 
Given an integer array nums, return an array answer such that answer[i] is equal to the product of all the elements of nums except nums[i].

The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit integer. */
func productExceptSelf(nums []int) []int {

	var product = 1
	var isZero = -1
	var isMultipleZero = -2
	for i, num := range nums {
		if num != 0 {
			product = product * num
		} else {
			isZero = i
			isMultipleZero++
		}
	}

	resultArr := make([]int, len(nums))
	if isMultipleZero >= 0 {
		return resultArr
	}
	if isZero != -1 {
		resultArr[isZero] = product
		return resultArr
	}
	for i, num := range nums {
		resultArr[i] = product / num
	}
	return resultArr
}
