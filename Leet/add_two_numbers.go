/**
problem statement: https://leetcode.com/problems/add-two-numbers/
*/
package leet

type ListNode struct {
	Val  int
	Next *ListNode
}

func insert(l *ListNode, num int) *ListNode {
	tempNode := &ListNode{
		Val: num,
	}
	if l == nil {
		//fmt.Println("nil")
		l = tempNode
		return l
	}
	ptr := l
	for ptr.Next != nil {
		ptr = ptr.Next
	}
	//fmt.Println(l)
	ptr.Next = tempNode
	return l
}

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	var l1Cnt, l2Cnt int
	place := 1

	for {
		l1Cnt += place * l1.Val
		place = place * 10
		l1 = l1.Next
		if l1 == nil {
			break
		}
	}

	place = 1

	for {
		l2Cnt += place * l2.Val
		place = place * 10
		l2 = l2.Next
		if l2 == nil {
			break
		}
	}

	l3Cnt := l1Cnt + l2Cnt

	var l3 *ListNode
	//l3 = nil
	//temp := &ListNode{}
	for {
		val := l3Cnt % 10
		l3Cnt = l3Cnt / 10
		l3 = insert(l3, val)

		if l3Cnt == 0 {
			break
		}

	}

	return l3

}
