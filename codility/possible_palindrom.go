package main

import (
	"fmt"
	"strings"
)

// you can also use imports, for example:
//import "fmt"
// import "os"

// you can write to stdout for debugging purposes, e.g.
// fmt.Println("this is a debug message")

func Solution(S string) []string {
	// write your code in Go 1.4
	//palindromStr := ""
	n := len(S) - 1
	byteStr := make([]string, len(S), len(S))
	//fmt.Println("printing bytearr",byteStr)
	for i := 0; i < len(S)/2; i++ {
		fmt.Println(byteStr)
		fmt.Println(S[i], S[n-i])
		if S[i] == S[n-i] {
			fmt.Println(S[i], S[n-i])
			if S[i] == '?' {
				fmt.Println(S[i], S[n-i])
				byteStr[i] = "a"
				byteStr[n-i] = "a"

			} else {
				byteStr[i] = string(S[i])
				byteStr[n-i] = string(S[n-i])
			}
		} else if S[i] == '?' {
			if S[n-i] == '?' {
				byteStr[i] = "a"
				byteStr[n-i] = "a"
			} else {
				byteStr[i] = string(S[n-i])
			}
		} else if S[n-i] == '?' {
			byteStr[n-i] = string(S[i])
		} else {
			return []string{"NO"}
		}
	}

	if n%2 == 0 {
		if S[n/2] == '?' {
			byteStr[n/2] = "a"
		} else {
			byteStr[n/2] = string(S[n/2])
		}

	}
	return byteStr
}

func main() {
	arr := Solution("?a?")
	fmt.Println(Solution("?a?"))
	str := strings.Join(arr, "")
	fmt.Println(str)
}
