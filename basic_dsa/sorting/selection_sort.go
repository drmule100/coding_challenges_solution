/*
  Selection sort: The selection sort algorithm sorts an array by repeatedly finding
  the minimum element (considering ascending order) from unsorted part and
  putting it at the beginning. The algorithm maintains two subarrays in a given array.

	1) The subarray which is already sorted.
	2) Remaining subarray which is unsorted. */

// loops 1 for 0->len
//    loop 2 for i+1 -> len
//       find index of smallest element and swap it with first element of unsorted array
package sorting
