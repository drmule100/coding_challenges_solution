/* 
  Bubble sort: Bubble Sort is the simplest sorting algorithm that works by repeatedly swapping the adjacent elements if they are in wrong order.
  logic is to find largest element and place it in last at each iteration
  */

 // loops: for i: 0->len
 //           for j: 0-> len-i  // last i elements are already sorted
 //                compare j and j+1 and swap in not in order
package sorting

