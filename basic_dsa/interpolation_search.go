/*
Interpolation search is an improved variant of binary search.
This search algorithm works on the probing position of the required value.
For this algorithm to work properly, the data collection should be in a sorted form and equally distributed.

Binary search has a huge advantage of time complexity over linear search.
Linear search has worst-case complexity of Ο(n) whereas binary search has Ο(log n).

There are cases where the location of target data may be known in advance.
For example, in case of a telephone directory, if we want to search the telephone number of Morphius.
Here, linear search and even binary search will seem slow as we can directly jump to memory space where the names start from 'M' are stored.
*/

// we can calculate mid = Low + ((High - Low) / (A[High] - A[Low])) * (Key - A[Low])
// Runtime complexity of interpolation search algorithm is Ο(log (log n)) as compared to Ο(log n) of BST in favorable situations.
package basicdsa

//todo implement it
