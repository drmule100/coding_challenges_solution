package basicdsa

import "fmt"

func search(data []int, key int) (int, bool) {

	for i, v := range data {
		if key == v {
			return i, true
		}
	}
	return -1, false
}

func main() {
	var data = []int{1, 2, 33, 22, 12, 54, 98, 67, 43, 23}
	var keys = []int{33, 3}

	for _, key := range keys {
		index, ok := search(data, key)
		if ok {
			fmt.Printf("key %d is found at index %d\n", key, index)
		} else {
			fmt.Printf("key %d is not found\n", key)
		}
	}

}
